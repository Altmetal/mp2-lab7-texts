#pragma once

#define MSG_OK 0
#define MSG_NO_ATOM 1
#define MSG_ERROR_NO_TEXT_LINK 2
#define MSG_ERROR_NO_FIRST_TEXT_LINK 3
#define MSG_ERROR_NO_CURRENT_TEXT_LINK 4
#define MSG_PATH_IS_EMPTY 5

class TDataCom {
protected:

    int _Message;

public:

    TDataCom() : _Message(MSG_OK) {}

    void SetMessage(int message) {
        _Message = message;
    }

    int GetMessage() {
        int m = _Message;
        _Message = MSG_OK;
        return m;
    }

};