set(target "views")

file(GLOB srcs "*.cpp")
file(GLOB hrcs "*.h")

#add_library(library STATIC ${SOURCE_LIB})
add_executable(${target} ${srcs} ${hdrs})

target_link_libraries(${target} src ${LIBRARY})